@REM ----------------------------------------------------------------------------
@REM  Copyright 2001-2006 The Apache Software Foundation.
@REM
@REM  Licensed under the Apache License, Version 2.0 (the "License");
@REM  you may not use this file except in compliance with the License.
@REM  You may obtain a copy of the License at
@REM
@REM       http://www.apache.org/licenses/LICENSE-2.0
@REM
@REM  Unless required by applicable law or agreed to in writing, software
@REM  distributed under the License is distributed on an "AS IS" BASIS,
@REM  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@REM  See the License for the specific language governing permissions and
@REM  limitations under the License.
@REM ----------------------------------------------------------------------------
@REM
@REM   Copyright (c) 2001-2006 The Apache Software Foundation.  All rights
@REM   reserved.

@echo off

set ERROR_CODE=0

:init
@REM Decide how to startup depending on the version of windows

@REM -- Win98ME
if NOT "%OS%"=="Windows_NT" goto Win9xArg

@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" @setlocal

@REM -- 4NT shell
if "%eval[2+2]" == "4" goto 4NTArgs

@REM -- Regular WinNT shell
set CMD_LINE_ARGS=%*
goto WinNTGetScriptDir

@REM The 4NT Shell from jp software
:4NTArgs
set CMD_LINE_ARGS=%$
goto WinNTGetScriptDir

:Win9xArg
@REM Slurp the command line arguments.  This loop allows for an unlimited number
@REM of arguments (up to the command line limit, anyway).
set CMD_LINE_ARGS=
:Win9xApp
if %1a==a goto Win9xGetScriptDir
set CMD_LINE_ARGS=%CMD_LINE_ARGS% %1
shift
goto Win9xApp

:Win9xGetScriptDir
set SAVEDIR=%CD%
%0\
cd %0\..\.. 
set BASEDIR=%CD%
cd %SAVEDIR%
set SAVE_DIR=
goto repoSetup

:WinNTGetScriptDir
set BASEDIR=%~dp0\..

:repoSetup
set REPO=


if "%JAVACMD%"=="" set JAVACMD=java

if "%REPO%"=="" set REPO=%BASEDIR%\repo

set CLASSPATH="%BASEDIR%"\etc;"%REPO%"\javax\servlet\javax.servlet-api\3.1.0\javax.servlet-api-3.1.0.jar;"%REPO%"\javax\servlet\jstl\1.2\jstl-1.2.jar;"%REPO%"\org\apache\tomcat\embed\tomcat-embed-core\8.5.2\tomcat-embed-core-8.5.2.jar;"%REPO%"\org\apache\tomcat\embed\tomcat-embed-logging-juli\8.5.2\tomcat-embed-logging-juli-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-jasper\8.5.2\tomcat-jasper-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-servlet-api\8.5.2\tomcat-servlet-api-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-juli\8.5.2\tomcat-juli-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-el-api\8.5.2\tomcat-el-api-8.5.2.jar;"%REPO%"\org\eclipse\jdt\core\compiler\ecj\4.5.1\ecj-4.5.1.jar;"%REPO%"\org\apache\tomcat\tomcat-api\8.5.2\tomcat-api-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-util-scan\8.5.2\tomcat-util-scan-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-util\8.5.2\tomcat-util-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-jasper-el\8.5.2\tomcat-jasper-el-8.5.2.jar;"%REPO%"\org\apache\tomcat\tomcat-jsp-api\8.5.2\tomcat-jsp-api-8.5.2.jar;"%REPO%"\commons-lang\commons-lang\2.6\commons-lang-2.6.jar;"%REPO%"\mysql\mysql-connector-java\6.0.6\mysql-connector-java-6.0.6.jar;"%REPO%"\net\sf\squirrel-sql\squirrelsql-dependency-plugin\1.0.7\squirrelsql-dependency-plugin-1.0.7.jar;"%REPO%"\org\apache\maven\maven-plugin-api\2.0\maven-plugin-api-2.0.jar;"%REPO%"\org\apache\maven\maven-project\2.0.2\maven-project-2.0.2.jar;"%REPO%"\org\apache\maven\maven-profile\2.0.2\maven-profile-2.0.2.jar;"%REPO%"\org\apache\maven\maven-model\2.0.2\maven-model-2.0.2.jar;"%REPO%"\org\apache\maven\maven-artifact-manager\2.0.2\maven-artifact-manager-2.0.2.jar;"%REPO%"\org\apache\maven\wagon\wagon-provider-api\1.0-alpha-6\wagon-provider-api-1.0-alpha-6.jar;"%REPO%"\org\codehaus\plexus\plexus-utils\1.1\plexus-utils-1.1.jar;"%REPO%"\org\apache\maven\maven-artifact\2.0.2\maven-artifact-2.0.2.jar;"%REPO%"\org\codehaus\plexus\plexus-container-default\1.0-alpha-9\plexus-container-default-1.0-alpha-9.jar;"%REPO%"\junit\junit\3.8.1\junit-3.8.1.jar;"%REPO%"\org\apache\maven\maven-settings\2.0.2\maven-settings-2.0.2.jar;"%REPO%"\org\apache\maven\plugins\maven-dependency-plugin\2.2\maven-dependency-plugin-2.2.jar;"%REPO%"\org\apache\maven\maven-core\2.0.9\maven-core-2.0.9.jar;"%REPO%"\org\apache\maven\wagon\wagon-file\1.0-beta-2\wagon-file-1.0-beta-2.jar;"%REPO%"\org\apache\maven\maven-plugin-parameter-documenter\2.0.9\maven-plugin-parameter-documenter-2.0.9.jar;"%REPO%"\org\apache\maven\wagon\wagon-webdav\1.0-beta-2\wagon-webdav-1.0-beta-2.jar;"%REPO%"\slide\slide-webdavlib\2.1\slide-webdavlib-2.1.jar;"%REPO%"\commons-httpclient\commons-httpclient\2.0.2\commons-httpclient-2.0.2.jar;"%REPO%"\jdom\jdom\1.0\jdom-1.0.jar;"%REPO%"\de\zeigermann\xml\xml-im-exporter\1.1\xml-im-exporter-1.1.jar;"%REPO%"\commons-logging\commons-logging\1.0.4\commons-logging-1.0.4.jar;"%REPO%"\org\apache\maven\wagon\wagon-http-lightweight\1.0-beta-2\wagon-http-lightweight-1.0-beta-2.jar;"%REPO%"\org\apache\maven\wagon\wagon-http-shared\1.0-beta-2\wagon-http-shared-1.0-beta-2.jar;"%REPO%"\jtidy\jtidy\4aug2000r7-dev\jtidy-4aug2000r7-dev.jar;"%REPO%"\xml-apis\xml-apis\1.0.b2\xml-apis-1.0.b2.jar;"%REPO%"\org\apache\maven\maven-error-diagnostics\2.0.9\maven-error-diagnostics-2.0.9.jar;"%REPO%"\commons-cli\commons-cli\1.0\commons-cli-1.0.jar;"%REPO%"\org\apache\maven\wagon\wagon-ssh-external\1.0-beta-2\wagon-ssh-external-1.0-beta-2.jar;"%REPO%"\org\apache\maven\wagon\wagon-ssh-common\1.0-beta-2\wagon-ssh-common-1.0-beta-2.jar;"%REPO%"\org\apache\maven\maven-plugin-descriptor\2.0.9\maven-plugin-descriptor-2.0.9.jar;"%REPO%"\org\codehaus\plexus\plexus-interactivity-api\1.0-alpha-4\plexus-interactivity-api-1.0-alpha-4.jar;"%REPO%"\org\apache\maven\maven-monitor\2.0.9\maven-monitor-2.0.9.jar;"%REPO%"\org\apache\maven\wagon\wagon-ssh\1.0-beta-2\wagon-ssh-1.0-beta-2.jar;"%REPO%"\com\jcraft\jsch\0.1.27\jsch-0.1.27.jar;"%REPO%"\org\apache\maven\maven-repository-metadata\2.0.9\maven-repository-metadata-2.0.9.jar;"%REPO%"\org\apache\maven\reporting\maven-reporting-api\3.0\maven-reporting-api-3.0.jar;"%REPO%"\org\apache\maven\reporting\maven-reporting-impl\2.0.5\maven-reporting-impl-2.0.5.jar;"%REPO%"\org\apache\maven\doxia\doxia-core\1.0\doxia-core-1.0.jar;"%REPO%"\org\apache\maven\shared\maven-doxia-tools\1.0.2\maven-doxia-tools-1.0.2.jar;"%REPO%"\commons-io\commons-io\1.4\commons-io-1.4.jar;"%REPO%"\commons-validator\commons-validator\1.2.0\commons-validator-1.2.0.jar;"%REPO%"\commons-beanutils\commons-beanutils\1.7.0\commons-beanutils-1.7.0.jar;"%REPO%"\commons-digester\commons-digester\1.6\commons-digester-1.6.jar;"%REPO%"\oro\oro\2.0.8\oro-2.0.8.jar;"%REPO%"\org\apache\maven\doxia\doxia-sink-api\1.0\doxia-sink-api-1.0.jar;"%REPO%"\org\apache\maven\doxia\doxia-site-renderer\1.0\doxia-site-renderer-1.0.jar;"%REPO%"\org\codehaus\plexus\plexus-i18n\1.0-beta-7\plexus-i18n-1.0-beta-7.jar;"%REPO%"\org\codehaus\plexus\plexus-velocity\1.1.7\plexus-velocity-1.1.7.jar;"%REPO%"\org\apache\velocity\velocity\1.5\velocity-1.5.jar;"%REPO%"\org\apache\maven\doxia\doxia-decoration-model\1.0\doxia-decoration-model-1.0.jar;"%REPO%"\org\apache\maven\doxia\doxia-module-apt\1.0\doxia-module-apt-1.0.jar;"%REPO%"\org\apache\maven\doxia\doxia-module-fml\1.0\doxia-module-fml-1.0.jar;"%REPO%"\org\apache\maven\doxia\doxia-module-xdoc\1.0\doxia-module-xdoc-1.0.jar;"%REPO%"\org\apache\maven\doxia\doxia-module-xhtml\1.0\doxia-module-xhtml-1.0.jar;"%REPO%"\org\codehaus\plexus\plexus-archiver\1.0-alpha-12\plexus-archiver-1.0-alpha-12.jar;"%REPO%"\org\apache\maven\shared\file-management\1.1\file-management-1.1.jar;"%REPO%"\org\apache\maven\shared\maven-shared-io\1.0\maven-shared-io-1.0.jar;"%REPO%"\org\codehaus\plexus\plexus-io\1.0-alpha-5\plexus-io-1.0-alpha-5.jar;"%REPO%"\org\apache\maven\shared\maven-dependency-analyzer\1.2\maven-dependency-analyzer-1.2.jar;"%REPO%"\asm\asm\3.0\asm-3.0.jar;"%REPO%"\org\apache\maven\shared\maven-dependency-tree\1.2\maven-dependency-tree-1.2.jar;"%REPO%"\org\apache\maven\shared\maven-common-artifact-filters\1.2\maven-common-artifact-filters-1.2.jar;"%REPO%"\org\apache\maven\shared\maven-plugin-testing-harness\1.1\maven-plugin-testing-harness-1.1.jar;"%REPO%"\org\apache\maven\shared\maven-invoker\2.0.7\maven-invoker-2.0.7.jar;"%REPO%"\commons-collections\commons-collections\3.2\commons-collections-3.2.jar;"%REPO%"\classworlds\classworlds\1.1\classworlds-1.1.jar;"%REPO%"\org\mindrot\jbcrypt\0.4\jbcrypt-0.4.jar;"%REPO%"\io\muic\ooc\webapp\login-webapp\1.0-SNAPSHOT\login-webapp-1.0-SNAPSHOT.jar

set ENDORSED_DIR=
if NOT "%ENDORSED_DIR%" == "" set CLASSPATH="%BASEDIR%"\%ENDORSED_DIR%\*;%CLASSPATH%

if NOT "%CLASSPATH_PREFIX%" == "" set CLASSPATH=%CLASSPATH_PREFIX%;%CLASSPATH%

@REM Reaching here means variables are defined and arguments have been captured
:endInit

%JAVACMD% %JAVA_OPTS%  -classpath %CLASSPATH% -Dapp.name="webapp" -Dapp.repo="%REPO%" -Dapp.home="%BASEDIR%" -Dbasedir="%BASEDIR%" io.muic.ooc.webapp.Webapp %CMD_LINE_ARGS%
if %ERRORLEVEL% NEQ 0 goto error
goto end

:error
if "%OS%"=="Windows_NT" @endlocal
set ERROR_CODE=%ERRORLEVEL%

:end
@REM set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" goto endNT

@REM For old DOS remove the set variables from ENV - we assume they were not set
@REM before we started - at least we don't leave any baggage around
set CMD_LINE_ARGS=
goto postExec

:endNT
@REM If error code is set to 1 then the endlocal was done already in :error.
if %ERROR_CODE% EQU 0 @endlocal


:postExec

if "%FORCE_EXIT_ON_ERROR%" == "on" (
  if %ERROR_CODE% NEQ 0 exit %ERROR_CODE%
)

exit /B %ERROR_CODE%
