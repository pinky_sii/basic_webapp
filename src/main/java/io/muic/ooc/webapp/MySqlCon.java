package io.muic.ooc.webapp;
import org.apache.commons.lang.StringUtils;
import org.mindrot.jbcrypt.BCrypt;

import javax.servlet.http.HttpServletRequest;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class MySqlCon {

    private static String url = "jdbc:mysql://localhost:3306/user?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
    private static String db_username = "root";
    private static String db_password = "pinky037";
    private static PreparedStatement preparedStatement;
    private static ResultSet resultSet;
    private static Connection connection;

    public static Connection getConnection(){
        try {

            Class.forName("com.mysql.cj.jdbc.Driver");
            if (connection == null || connection.isClosed()) {
                connection = DriverManager.getConnection(url, db_username, db_password);
            }

        } catch (Exception e) {
            System.out.println(e);
        }

        if (connection == null) {
            System.out.println("Connection is NULL!");
        }

        return connection;
    }

    public static boolean isAuthenticate(String username, String password,HttpServletRequest request ) throws SQLException {

        String query = "SELECT password FROM user_info WHERE username = ?;";

        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1,username);
        resultSet = preparedStatement.executeQuery();

        System.out.println("enter while loop");

        while (resultSet.next()){

            String dbPassword = resultSet.getString("password");

            request.getSession().setAttribute("username", username);
            return BCrypt.checkpw(password, dbPassword);
        }
        return false;
    }

    public static void addUser(String username, String password, String firstname, String lastname, String email){

        String query = "INSERT INTO user_info(username,password,firstname,lastname,email) VALUES (?,?,?,?,?);";
        password = BCrypt.hashpw(password, BCrypt.gensalt());

        try {

            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1,username);
            preparedStatement.setString(2,password);
            preparedStatement.setString(3,firstname);
            preparedStatement.setString(4,lastname);
            preparedStatement.setString(5,email);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static boolean isAlreadyExist(String col, String username) throws SQLException {

        String qyery = "SELECT " + col +" FROM user_info WHERE " + col + " = ?;";

        try {
            preparedStatement = getConnection().prepareStatement(qyery);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        preparedStatement.setString(1,username);
        resultSet = preparedStatement.executeQuery();

        if (resultSet.first()) {
            return true;
        }
        return false;
    }


    public static User getUser(String username) throws SQLException {

        User user = new User();
        String query = "SELECT * FROM user_info WHERE username= ?;";

        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1,username);
        resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setFirstname(resultSet.getString("firstname"));
            user.setLastname(resultSet.getString("lastname"));
            user.setEmail(resultSet.getString("email"));
        }
        return user;
    }

    public static List<User> getUserList() throws SQLException {

        List<User> userList = new ArrayList<>();
        String query = "SELECT * from user_info;";

        preparedStatement = getConnection().prepareStatement(query);
        resultSet = preparedStatement.executeQuery();

        while (resultSet.next()) {
            User user = new User();
            user.setUsername(resultSet.getString("username"));
            user.setPassword(resultSet.getString("password"));
            user.setFirstname(resultSet.getString("firstname"));
            user.setLastname(resultSet.getString("lastname"));
            user.setEmail(resultSet.getString("email"));
            userList.add(user);
        }
        return userList;
    }

    public static void removeUser(String username) throws SQLException {

        System.out.println("Delete: "+ username);
        try {

            String query = "DELETE FROM user_info WHERE username = ?;";
            preparedStatement = getConnection().prepareStatement(query);
            preparedStatement.setString(1,username);
            preparedStatement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public static void upDate(String col, String username, String updateInfo) throws SQLException {

        String query = "UPDATE user_info SET " + col + " = ? WHERE username = ?;";

        if (col.equals("password")){
            updateInfo = BCrypt.hashpw(updateInfo, BCrypt.gensalt());
        }

        preparedStatement = getConnection().prepareStatement(query);
        preparedStatement.setString(1,updateInfo);
        preparedStatement.setString(2,username);
        preparedStatement.executeUpdate();

    }
}
