package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.MySqlCon;
import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.service.SecurityService;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class RemoveServlet extends HttpServlet implements Routable{

    private SecurityService securityService;

    @Override
    public String getMapping() {
        return "/remove" ;
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String user = req.getParameter("username");
        System.out.println("getParameter: " + user);

        try {

            MySqlCon.removeUser(user);

        } catch (SQLException e) {
            e.printStackTrace();
        }
        resp.sendRedirect("/user");
    }

}
