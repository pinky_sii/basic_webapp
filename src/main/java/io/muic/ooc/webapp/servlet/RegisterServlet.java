package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.MySqlCon;
import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.service.SecurityService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

/*
 *  Siripatsorn S.
 *  March 7, 2018
 */

public class RegisterServlet extends HttpServlet implements Routable {

    private SecurityService securityService;


    @Override
    public String getMapping() {
        return "/register";
    }

    @Override
    public void setSecurityService(SecurityService securityService) { this.securityService = securityService; }


    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/register.jsp");
        rd.include(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String firstname = request.getParameter("firstname");
        String lastname = request.getParameter("lastname");
        String email = request.getParameter("email");

        if (StringUtils.isBlank(username) || StringUtils.isBlank(password) ||
                !StringUtils.isAlphanumeric(username) || !StringUtils.isAlphanumeric(password)) {
                response.sendRedirect("/register?status=fail");

        } else {

            try {
                if (MySqlCon.isAlreadyExist("username",username)) {

                    String error = "This username is already exist";
                    request.setAttribute("error", error);

                    RequestDispatcher rd = request.getRequestDispatcher("WEB-INF/register.jsp");
                    rd.include(request, response);

                } else {
                    // add new user to database
                    MySqlCon.addUser(username,password,firstname,lastname,email);
                    response.sendRedirect("/login?register=success");
                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
