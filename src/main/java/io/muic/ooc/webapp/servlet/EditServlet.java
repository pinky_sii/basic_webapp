package io.muic.ooc.webapp.servlet;

import io.muic.ooc.webapp.MySqlCon;
import io.muic.ooc.webapp.Routable;
import io.muic.ooc.webapp.service.SecurityService;
import org.apache.commons.lang.StringUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

public class EditServlet extends HttpServlet implements Routable {

    private SecurityService securityService;

    @Override
    public String getMapping() {
        return "/edit";
    }

    @Override
    public void setSecurityService(SecurityService securityService) {
        this.securityService = securityService;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/edit.jsp");
        rd.include(req, resp);
        boolean authorized = securityService.isAuthorized(req);
        if (!authorized) {
            resp.sendRedirect("/login");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        boolean authorized = securityService.isAuthorized(req);

        if (!authorized) {
            String error = "Please log in";
            req.setAttribute("error", error);
            RequestDispatcher rd = req.getRequestDispatcher("WEB-INF/login.jsp");
            resp.sendRedirect("/login");
        }

        String username = req.getParameter("username");
        String password = req.getParameter("new_password");
        String firstname = req.getParameter("new_fname");
        String lastname = req.getParameter("new_lname");
        String email = req.getParameter("new_email");

        System.out.println("---EDIT---");
        System.out.println(username);
        System.out.println("first name" + firstname);
        System.out.println("last name" + lastname);
        System.out.println("email " + email);

        try {
            if (MySqlCon.isAlreadyExist("email",email)){

                System.out.println("Duplicate");
                String error = "This email is already exist.";
                req.setAttribute("error", error);
                resp.sendRedirect("/edit?editUser=" + username +"&&status=invalidemail");

            } else {

                if (!StringUtils.isBlank(firstname)) {
                    MySqlCon.upDate("firstname", username, firstname);
                }

                if (!StringUtils.isBlank(lastname)) {
                    MySqlCon.upDate("lastName", username, lastname);
                }
                if (!StringUtils.isBlank(email) ) {
                    MySqlCon.upDate("email", username, email);
                }

                if (!StringUtils.isBlank(password)) {
                    MySqlCon.upDate("password", username, password);
                }
                resp.sendRedirect("/user?edit=success&&user="+username);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }


    }
}
