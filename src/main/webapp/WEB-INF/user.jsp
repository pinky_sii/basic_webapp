<%@ page import="io.muic.ooc.webapp.User" %>
<%@ page import="java.util.List" %>
<%@ page import="io.muic.ooc.webapp.MySqlCon" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>


<html>
<title>User List</title>
<head>
    <style>
        body {
            background-color: lightblue;
        }

        h2 {
            color: white;
        }

    </style>
</head>


<body>



    <script type='text/javascript'>
        function confirmDel() {
            return confirm("Are you sure to delete this user?");
        }
    </script>

    <h2>Welcome, ${username}</h2>


    <table border="1">
        <tr>
            <th>User</th>
            <th>First name</th>
            <th>Last name</th>
            <th>E-mail</th>
            <th> </th>

        </tr>

        <%

            String currentUser = (String) request.getSession().getAttribute("username");
            List<User> userList = MySqlCon.getUserList();

            for (User user: userList)
            {

        %>


        <tr>

            <td><%=user.getUsername()%></td>
            <td><%=user.getFirstname()%></td>
            <td><%=user.getLastname()%></td>
            <td><%=user.getEmail()%></td>

            <td>

                    <form action="/edit" method="GET">
                        <input type="hidden" name="editUser" value="<%=user.getUsername()%>" >
                        <input type="submit" value="Edit">
                    </form>

                    <%
                        if(user.getUsername().equals(currentUser)){
                    %>
                    <input type="submit" value="Delete" disabled="disabled">

                    <%
                    } else {
                    %>

                    <form onsubmit="return confirmDel()" action="/remove" method="GET">
                        <input type="hidden" name="username" value="<%=user.getUsername()%>" >
                        <input type="submit" value="remove">
                    </form>

                <%
                    }
                %>

            </td>

        </tr>

        <%
            }
        %>

    </table>


    <form action="/logout" method="GET">
        <input type="submit" value="Log out">
    </form>

    <form action="/register" method="GET">
        <input type="submit" value="Add user">
    </form>


</body>

</html>
